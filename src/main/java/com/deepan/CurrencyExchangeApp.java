package com.deepan;

import com.google.gson.*;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.util.Scanner;

public class CurrencyExchangeApp {
    public static void main(String[] args) {
        System.out.println("Welcome to currency Exchange App");
        System.out.println("=====================================");

        Scanner scanner = new Scanner(System.in);
        //        sending request
        OkHttpClient client = new OkHttpClient().newBuilder().build();

        Gson gson = new Gson();

        System.out.println("Enter the source Currency");
        String from = scanner.next();
        System.out.println("Enter the currency you want to convert to");
        String to = scanner.next();
        System.out.println("Enter the amount");
        String amount = scanner.next();
        String url = "https://api.apilayer.com/exchangerates_data/convert?from=" + from + "&to=" + to + "&amount=" + amount;
        String apiKey = "GfRrzo6glVur4K5kJAsQxGWhmAKi1U4x";


//        building request

        Request request = new Request.Builder()
                .url(url)
                .method("GET", null)
                .addHeader("apiKey", apiKey)
                .build();

        try {
            Response response = client.newCall(request).execute();
            String result = response.body().string();

            JsonObject resultObject = gson.fromJson(result, JsonObject.class);

            JsonPrimitive convertedValue = resultObject.getAsJsonPrimitive("result");

            System.out.println("Coverted Value = "+convertedValue);

        } catch (Exception e) {

            System.out.println("Server Request Failed");
        }


//        read to
//        read from
//        read amount

//        1. request paramters
//        2. apikey
    }
}
