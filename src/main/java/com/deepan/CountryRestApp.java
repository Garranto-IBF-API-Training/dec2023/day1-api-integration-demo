package com.deepan;

import com.google.gson.*;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class CountryRestApp {

    public static void main(String[] args) {
        System.out.println("Country App");
        System.out.println("===============");

//        sending request
        OkHttpClient client = new OkHttpClient().newBuilder().build();

        Gson gson = new Gson();

//        building request

        Request request = new Request.Builder()
                .url("https://restcountries.com/v3.1/all")
                .method("GET",null)
                .build();

        try{
          Response response = client.newCall(request).execute();
          String countries = response.body().string();

            JsonArray countryArray = gson.fromJson(countries, JsonArray.class);

            System.out.println("No of countries:"+countryArray.size());


            for(JsonElement country: countryArray){

               JsonObject countryObject = country.getAsJsonObject();

               JsonObject nameObject = countryObject.getAsJsonObject("name");

                JsonPrimitive countryOfficialName = nameObject.getAsJsonPrimitive("official");

                System.out.println(countryOfficialName);
            }

        }catch (Exception e){

            System.out.println("Server Request Failed");
        }

    }
}

//1. country app - integrate with country rest
//
//2. currency converter app - exchange rate rest

//1. prepare the request
//2. send request to endpoint (okhhtp)
//3. access the response
//4 parse the response "string to java object" (GSON)
//5. process the data as required (display the names in the console)

//create html table displaying country info

//there is no println
//there is no country rest

//there is no currency converter api


//google maps api

//endpoint

//JSONElement
//JsonArray
//JsonObject
//JSOnPrimitive





